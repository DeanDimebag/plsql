
--CREATE VIEW FOR TOP 20 COUNTRIES
create or replace view vw_top_20 as 
select c_position, country_name, total_cases from(
select row_number() over (order by total_cases desc) c_position, country_name, total_cases
from tbl_coronavirus)
where c_position <21; 

select * from vw_top_20;

--CREATE VIEW FOR BOTTOM 20 COUNTIRES
create or replace view vw_bottom_20 as 
select c_position, country_name, total_cases from (select row_number() over (order by total_cases asc) c_position, country_name, total_cases
from tbl_coronavirus)
where c_position<21;

select * from vw_bottom_20;


