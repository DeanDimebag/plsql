(async () => {
    const parseLine = (a) => a.replace(/"([0-9,]+)"/g, (_, p1)=> p1.split(',').join(''));
    const fillVoid = (line) => line.map((v, i) => v.length === 0 ? null : i === 0 ? `'${v}'`: v);
    const filterFields = [0,2,4,5,7];
    const filterFunc = (ar) => ar.filter((_, i) => filterFields.includes(i))
    const outputFile = require("fs").createWriteStream("./latest.sql", { encoding:'utf8' });
    
    let rl= require("readline").createInterface({
        input: require("fs").createReadStream("./latest.csv")
    });
    let first_line = true;
    for await (let line of rl) {
        if (first_line)  {
            first_line = false;
            continue;
        }
        let f = filterFunc(fillVoid(parseLine(line).split(','))).join(",");
        outputFile.write(`exec sp_insert_update (${f});\n`);
    }
})();