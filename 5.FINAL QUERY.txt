SELECT vt.country_name, vt.total_cases, a.txt, b.txt1, c.txt2, d.txt3, e.txt4, f.txt5, g.txt6,  h.txt7, vb.country_name, vb.total_cases FROM vw_top_20 vt join vw_bottom_20 vb on vt.c_position = vb.c_position
LEFT JOIN (
SELECT 2 sn, 'Total Case:' txt FROM dual
UNION
SELECT 3, ''|| SUM(total_cases) FROM tbl_coronavirus
UNION
SELECT 10, 'Active' FROM dual
UNION
SELECT 11, 'Cases' FROM dual
) a 
on vt.c_position=a.sn 

LEFT JOIN (
SELECT 14 sn, ''|| (SUM(total_cases) - SUM(deaths) - SUM(recovered) - SUM(serious)) || 
'(' || ROUND( ( SUM(total_cases) - SUM(deaths) - SUM(recovered) - SUM(serious) ) / (SUM(total_cases) - SUM(deaths) - SUM(recovered))*100, 1 ) || '%)' txt1 FROM TBL_CORONAVIRUS 
UNION
SELECT 15, 'in Mild Condition' FROM dual
) b 
on vt.c_position = b.sn
LEFT JOIN (
SELECT 10 sn, ''|| (SUM(total_cases)-SUM(deaths)-SUM(recovered)) txt2 FROM tbl_coronavirus
UNION
SELECT 11, 'Currently Effected Patients' FROM dual
UNION 
SELECT 19, 'Nepal position : ' || func_position_finder('nepal') ||' - ' FROM dual
) c 
on vt.c_position= c.sn
LEFT JOIN ( 
SELECT 14 sn, ''|| SUM(serious)|| '(' || ROUND((SUM(SERIOUS)/(SUM(total_cases)-SUM(deaths)-SUM(recovered)))*100,1) || '%)' txt3 FROM tbl_coronavirus
UNION
SELECT 15, 'Serious or Critical' FROM dual
UNION
SELECT 19, 'Total :'|| SUM(total_cases) ||' ; ' FROM tbl_coronavirus
) d
on vt.c_position= d.sn
LEFT JOIN ( 
SELECT 2 sn, 'Deaths :' txt4 FROM dual
UNION
SELECT3, ''|| SUM(deaths) FROM tbl_coronavirus
UNION
SELECT 19, 'today : '|| new_cases ||' - ' FROM tbl_coronavirus
) e
on vt.c_position= e.sn
LEFT JOIN (
SELECT 14 sn, ''|| SUM(recovered) ||'('|| Round(SUM(recovered)/(SUM(deaths)+SUM(recovered))*100) ||'%)' txt5 FROM tbl_coronavirus
UNION
SELECT 15, 'Recovered/Discharged' FROM dual
UNION
SELECT 19, 'deaths :'|| SUM(deaths) || ' ; ' FROM tbl_coronavirus
) f
on vt.c_position = f.sn
LEFT JOIN (
SELECT 2 sn, 'Recovered:' txt6 FROM dual
UNION
SELECT 3, ''|| SUM(recovered) FROM tbl_coronavirus
UNION
SELECT 8, 'Closed Cases:' FROM dual
UNION
SELECT 10, ''|| (SUM(deaths)+SUM(recovered)) FROM tbl_coronavirus
UNION
SELECT 11, 'Cases which had an outcomes' FROM dual
UNION
SELECT 19, 'today : '|| new_deaths FROM tbl_coronavirus
) g
on vt.c_position=g.sn 
LEFT JOIN (
SELECT 14 sn, ''||SUM(deaths) ||'('|| Round(SUM(deaths)/(SUM(deaths)+SUM(recovered))*100) ||'%)' txt7 FROM tbl_coronavirus
UNION
SELECT 15, 'Deaths' FROM dual
) h
on vt.c_position = h.sn
order by vt.c_position asc;
